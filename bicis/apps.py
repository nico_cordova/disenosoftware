from django.apps import AppConfig


class BicisConfig(AppConfig):
    name = 'bicis'
