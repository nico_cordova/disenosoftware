# -*- coding: utf-8 -*-
RESERVA_STATUS_CHOICES = (
    ('EA', 'Entregado Atrasado'),
    ('EN', 'Entregado'),
    ('AT', 'Atrasado'),
    ('AC', 'Activo'),
)

RESERVA_STATUS_DEFAULT = "AC"