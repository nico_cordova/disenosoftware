from django.contrib import admin
from django import forms
from bicis.models import Sucursal,Equipo,Reserva,ReservaEquipo,Casco,Bicicleta,SeguridadArticular
# Register your models here.
class SucursalForm(forms.ModelForm):
    class Meta:
        model = Sucursal
        fields = '__all__'


class SucursalAdmin(admin.ModelAdmin):
    form = SucursalForm
    list_display = ['direccion','telefono']

admin.site.register(Sucursal, SucursalAdmin)

class ReservaForm(forms.ModelForm):
    class Meta:
        model = Reserva
        fields = '__all__'


class ReservaAdmin(admin.ModelAdmin):
    form = ReservaForm
    list_display = ['fecha','fecha_limite','get_cant_productos','get_string_productos']

admin.site.register(Reserva, ReservaAdmin)

class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = '__all__'


class EquipoAdmin(admin.ModelAdmin):
    form = EquipoForm
    list_display = ['marca','modelo','stock','ult_abastecimiento']

admin.site.register(Equipo, EquipoAdmin)

class ReservaEquipoForm(forms.ModelForm):
    class Meta:
        model = ReservaEquipo
        fields = '__all__'


class ReservaEquipoAdmin(admin.ModelAdmin):
    form = ReservaEquipoForm
    list_display = ['cant','reserva','equipo']

admin.site.register(ReservaEquipo, ReservaEquipoAdmin)

class CascoForm(forms.ModelForm):
    class Meta:
        model = Casco
        fields = '__all__'

class CascoAdmin(admin.ModelAdmin):
    form = CascoForm
    list_display = ['material','dimension','marca','modelo','stock','ult_abastecimiento']

admin.site.register(Casco, CascoAdmin)

class BicicletaForm(forms.ModelForm):
    class Meta:
        model = Bicicleta
        fields = '__all__'

class BicicletaAdmin(admin.ModelAdmin):
    form = BicicletaForm
    list_display = ['material','aro','marca','modelo','stock','ult_abastecimiento']

admin.site.register(Bicicleta, BicicletaAdmin)

class SeguridadArticularForm(forms.ModelForm):
    class Meta:
        model = SeguridadArticular
        fields = '__all__'

class SeguridadArticularAdmin(admin.ModelAdmin):
    form = SeguridadArticularForm
    list_display = ['material_tela','material_proteccion','marca','modelo','stock','ult_abastecimiento']

admin.site.register(SeguridadArticular, SeguridadArticularAdmin)