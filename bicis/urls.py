from django.urls import path
from bicis import views

urlpatterns = [
    path('', views.index, name="index"),
]
