from django.db import models
from django.utils import timezone
from bicis.defines import RESERVA_STATUS_CHOICES,RESERVA_STATUS_DEFAULT

class Reserva(models.Model):
    fecha = models.DateTimeField(default=timezone.now)
    fecha_entrega = models.DateTimeField(default=timezone.now)
    fecha_limite = models.DateTimeField(default=timezone.now)
    estado = models.CharField(max_length=2,choices=RESERVA_STATUS_CHOICES,default=RESERVA_STATUS_DEFAULT)
    encargado = models.ForeignKey("bici_auth.Encargado", null=True, on_delete=models.SET_NULL)
    cliente = models.ForeignKey("bici_auth.Cliente", null=True, on_delete=models.SET_NULL)

    def get_cant_productos(self):
        output = 0
        for producto in self.reservaequipo_set.all():
            output += producto.cant

        return output

    def get_string_productos(self):
        output = ""
        for producto in self.reservaequipo_set.all():
            output += 'Producto: {0} Cantidad: {1};'.format(self.producto,self.stock)
        
        return output
    
    def __str__(self):
        return "Solicitante: {0}, Fecha Solicitud: {1}, Pedido: {2}".format(self.cliente, self.fecha, self.get_string_productos)

class Sucursal(models.Model):
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=50)
    horario_apertura = models.TimeField(default=timezone.now)
    def __str__(self):
        return "Dirección: {0},Teléfono: {1}".format(self.direccion,self.telefono)

class Equipo(models.Model):
    marca = models.CharField(max_length=100, default="No Indica")
    modelo = models.CharField(max_length=100, default="No Indica")
    stock = models.IntegerField(default=1)
    ult_abastecimiento = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return "%s %s" % (self.marca, self.modelo)

class ReservaEquipo(models.Model):
    cant = models.IntegerField(default=1)
    reserva = models.ForeignKey(Reserva, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo,on_delete=models.CASCADE)

    def __str__(self):
        return "{0}, cantidad: {1}".format(self.equipo,)

class Casco(Equipo):
    material = models.CharField(max_length=60, default="No define")
    dimension = models.CharField(max_length=20, default="No define")
    def __str__(self):
        return "%s %s" % (self.marca, self.modelo)

class Bicicleta(Equipo):
    aro = models.IntegerField(default=1)
    material = models.CharField(max_length=60, default="No define")
    def __str__(self):
        return "%s %s" % (self.marca, self.modelo)
    
class SeguridadArticular(Equipo):
    material_tela = models.CharField(max_length=60, default="No define")
    material_proteccion = models.CharField(max_length=60, default="No define")
    def __str__(self):
        return "%s %s" % (self.marca, self.modelo)
