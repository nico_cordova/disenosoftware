-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: biciDB
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add group',2,'add_group'),(6,'Can change group',2,'change_group'),(7,'Can delete group',2,'delete_group'),(8,'Can view group',2,'view_group'),(9,'Can add permission',3,'add_permission'),(10,'Can change permission',3,'change_permission'),(11,'Can delete permission',3,'delete_permission'),(12,'Can view permission',3,'view_permission'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add cliente',7,'add_cliente'),(26,'Can change cliente',7,'change_cliente'),(27,'Can delete cliente',7,'delete_cliente'),(28,'Can view cliente',7,'view_cliente'),(29,'Can add encargado',8,'add_encargado'),(30,'Can change encargado',8,'change_encargado'),(31,'Can delete encargado',8,'delete_encargado'),(32,'Can view encargado',8,'view_encargado'),(33,'Can add equipo',9,'add_equipo'),(34,'Can change equipo',9,'change_equipo'),(35,'Can delete equipo',9,'delete_equipo'),(36,'Can view equipo',9,'view_equipo'),(37,'Can add casco',10,'add_casco'),(38,'Can change casco',10,'change_casco'),(39,'Can delete casco',10,'delete_casco'),(40,'Can view casco',10,'view_casco'),(41,'Can add seguridad articular',11,'add_seguridadarticular'),(42,'Can change seguridad articular',11,'change_seguridadarticular'),(43,'Can delete seguridad articular',11,'delete_seguridadarticular'),(44,'Can view seguridad articular',11,'view_seguridadarticular'),(45,'Can add reserva',12,'add_reserva'),(46,'Can change reserva',12,'change_reserva'),(47,'Can delete reserva',12,'delete_reserva'),(48,'Can view reserva',12,'view_reserva'),(49,'Can add sucursal',13,'add_sucursal'),(50,'Can change sucursal',13,'change_sucursal'),(51,'Can delete sucursal',13,'delete_sucursal'),(52,'Can view sucursal',13,'view_sucursal'),(53,'Can add reserva equipo',14,'add_reservaequipo'),(54,'Can change reserva equipo',14,'change_reservaequipo'),(55,'Can delete reserva equipo',14,'delete_reservaequipo'),(56,'Can view reserva equipo',14,'view_reservaequipo'),(57,'Can add bicicleta',15,'add_bicicleta'),(58,'Can change bicicleta',15,'change_bicicleta'),(59,'Can delete bicicleta',15,'delete_bicicleta'),(60,'Can view bicicleta',15,'view_bicicleta');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$120000$aM8Yye0qGgIs$ZdqvUS1rl75oZuj+z/G8Do/Wo8UdTed/kLzoE0uLTiU=','2018-11-19 01:38:39.655293',1,'nico','Nicolás Andree','Córdova Peña y lillo','',1,1,'2018-11-18 21:53:26.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bici_auth_cliente`
--

DROP TABLE IF EXISTS `bici_auth_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bici_auth_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefono` varchar(16) NOT NULL,
  `rut` varchar(12) NOT NULL,
  `num_tarjeta` int(11) NOT NULL,
  `Mven_tarjeta` int(11) NOT NULL,
  `Aven_tarjeta` int(11) NOT NULL,
  `codigo_seguridad` int(11) NOT NULL,
  `fecha_ingreso` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bici_auth_cliente_user_id_92d895f3_uniq` (`user_id`),
  CONSTRAINT `bici_auth_cliente_user_id_92d895f3_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bici_auth_cliente`
--

LOCK TABLES `bici_auth_cliente` WRITE;
/*!40000 ALTER TABLE `bici_auth_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `bici_auth_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bici_auth_encargado`
--

DROP TABLE IF EXISTS `bici_auth_encargado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bici_auth_encargado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefono` varchar(16) NOT NULL,
  `rut` varchar(12) NOT NULL,
  `fecha_contrato` datetime(6) NOT NULL,
  `tipo_encargado` varchar(2) NOT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bici_auth_encargado_user_id_0b1fc492_uniq` (`user_id`),
  KEY `bici_auth_encargado_sucursal_id_542c92c8_fk_bicis_sucursal_id` (`sucursal_id`),
  CONSTRAINT `bici_auth_encargado_sucursal_id_542c92c8_fk_bicis_sucursal_id` FOREIGN KEY (`sucursal_id`) REFERENCES `bicis_sucursal` (`id`),
  CONSTRAINT `bici_auth_encargado_user_id_0b1fc492_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bici_auth_encargado`
--

LOCK TABLES `bici_auth_encargado` WRITE;
/*!40000 ALTER TABLE `bici_auth_encargado` DISABLE KEYS */;
INSERT INTO `bici_auth_encargado` VALUES (2,'+56965168608','18295315-6','2018-11-18 23:52:40.000000','ET',1,1);
/*!40000 ALTER TABLE `bici_auth_encargado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_bicicleta`
--

DROP TABLE IF EXISTS `bicis_bicicleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_bicicleta` (
  `equipo_ptr_id` int(11) NOT NULL,
  `aro` int(11) NOT NULL,
  `material` varchar(60) NOT NULL,
  PRIMARY KEY (`equipo_ptr_id`),
  CONSTRAINT `bicis_bicicleta_equipo_ptr_id_57d89694_fk_bicis_equipo_id` FOREIGN KEY (`equipo_ptr_id`) REFERENCES `bicis_equipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_bicicleta`
--

LOCK TABLES `bicis_bicicleta` WRITE;
/*!40000 ALTER TABLE `bicis_bicicleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_bicicleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_casco`
--

DROP TABLE IF EXISTS `bicis_casco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_casco` (
  `equipo_ptr_id` int(11) NOT NULL,
  `material` varchar(60) NOT NULL,
  `dimension` varchar(20) NOT NULL,
  PRIMARY KEY (`equipo_ptr_id`),
  CONSTRAINT `bicis_casco_equipo_ptr_id_378a00b2_fk_bicis_equipo_id` FOREIGN KEY (`equipo_ptr_id`) REFERENCES `bicis_equipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_casco`
--

LOCK TABLES `bicis_casco` WRITE;
/*!40000 ALTER TABLE `bicis_casco` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_casco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_equipo`
--

DROP TABLE IF EXISTS `bicis_equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_equipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `ult_abastecimiento` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_equipo`
--

LOCK TABLES `bicis_equipo` WRITE;
/*!40000 ALTER TABLE `bicis_equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_reserva`
--

DROP TABLE IF EXISTS `bicis_reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime(6) NOT NULL,
  `fecha_entrega` datetime(6) NOT NULL,
  `fecha_limite` datetime(6) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `encargado_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bicis_reserva_cliente_id_9a813872_fk_bici_auth_cliente_id` (`cliente_id`),
  KEY `bicis_reserva_encargado_id_99bac518_fk_bici_auth_encargado_id` (`encargado_id`),
  CONSTRAINT `bicis_reserva_cliente_id_9a813872_fk_bici_auth_cliente_id` FOREIGN KEY (`cliente_id`) REFERENCES `bici_auth_cliente` (`id`),
  CONSTRAINT `bicis_reserva_encargado_id_99bac518_fk_bici_auth_encargado_id` FOREIGN KEY (`encargado_id`) REFERENCES `bici_auth_encargado` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_reserva`
--

LOCK TABLES `bicis_reserva` WRITE;
/*!40000 ALTER TABLE `bicis_reserva` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_reservaequipo`
--

DROP TABLE IF EXISTS `bicis_reservaequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_reservaequipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cant` int(11) NOT NULL,
  `equipo_id` int(11) NOT NULL,
  `reserva_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bicis_reservaequipo_equipo_id_c294a212_fk_bicis_equipo_id` (`equipo_id`),
  KEY `bicis_reservaequipo_reserva_id_fbef9046_fk_bicis_reserva_id` (`reserva_id`),
  CONSTRAINT `bicis_reservaequipo_equipo_id_c294a212_fk_bicis_equipo_id` FOREIGN KEY (`equipo_id`) REFERENCES `bicis_equipo` (`id`),
  CONSTRAINT `bicis_reservaequipo_reserva_id_fbef9046_fk_bicis_reserva_id` FOREIGN KEY (`reserva_id`) REFERENCES `bicis_reserva` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_reservaequipo`
--

LOCK TABLES `bicis_reservaequipo` WRITE;
/*!40000 ALTER TABLE `bicis_reservaequipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_reservaequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_seguridadarticular`
--

DROP TABLE IF EXISTS `bicis_seguridadarticular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_seguridadarticular` (
  `equipo_ptr_id` int(11) NOT NULL,
  `material_tela` varchar(60) NOT NULL,
  `material_proteccion` varchar(60) NOT NULL,
  PRIMARY KEY (`equipo_ptr_id`),
  CONSTRAINT `bicis_seguridadartic_equipo_ptr_id_3cfc2cc3_fk_bicis_equ` FOREIGN KEY (`equipo_ptr_id`) REFERENCES `bicis_equipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_seguridadarticular`
--

LOCK TABLES `bicis_seguridadarticular` WRITE;
/*!40000 ALTER TABLE `bicis_seguridadarticular` DISABLE KEYS */;
/*!40000 ALTER TABLE `bicis_seguridadarticular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bicis_sucursal`
--

DROP TABLE IF EXISTS `bicis_sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bicis_sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `horario_apertura` time(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicis_sucursal`
--

LOCK TABLES `bicis_sucursal` WRITE;
/*!40000 ALTER TABLE `bicis_sucursal` DISABLE KEYS */;
INSERT INTO `bicis_sucursal` VALUES (1,'Tránsito 5555','+56965168608','02:46:32.211515');
/*!40000 ALTER TABLE `bicis_sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-11-18 22:17:36.590744','1','nico',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\"]}}]',4,1),(2,'2018-11-18 23:52:32.556944','1','Dirección: Tránsito 5555,Teléfono: +56965168608',1,'[{\"added\": {}}]',13,1),(3,'2018-11-18 23:54:30.441180','2','Nicolás Andree Córdova Peña y lillo',1,'[{\"added\": {}}]',8,1),(4,'2018-11-19 01:33:14.683167','2','Nicolás Andree Córdova Peña y lillo',2,'[{\"changed\": {\"fields\": [\"user_type\"]}}]',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(2,'auth','group'),(3,'auth','permission'),(4,'auth','user'),(15,'bicis','bicicleta'),(10,'bicis','casco'),(9,'bicis','equipo'),(12,'bicis','reserva'),(14,'bicis','reservaequipo'),(11,'bicis','seguridadarticular'),(13,'bicis','sucursal'),(7,'bici_auth','cliente'),(8,'bici_auth','encargado'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-11-18 20:46:14.933688'),(2,'auth','0001_initial','2018-11-18 20:46:15.132642'),(3,'admin','0001_initial','2018-11-18 20:46:15.196199'),(4,'admin','0002_logentry_remove_auto_add','2018-11-18 20:46:15.231708'),(5,'admin','0003_logentry_add_action_flag_choices','2018-11-18 20:46:15.245343'),(6,'contenttypes','0002_remove_content_type_name','2018-11-18 20:46:15.303671'),(7,'auth','0002_alter_permission_name_max_length','2018-11-18 20:46:15.313582'),(8,'auth','0003_alter_user_email_max_length','2018-11-18 20:46:15.357006'),(9,'auth','0004_alter_user_username_opts','2018-11-18 20:46:15.369667'),(10,'auth','0005_alter_user_last_login_null','2018-11-18 20:46:15.403036'),(11,'auth','0006_require_contenttypes_0002','2018-11-18 20:46:15.406382'),(12,'auth','0007_alter_validators_add_error_messages','2018-11-18 20:46:15.422220'),(13,'auth','0008_alter_user_username_max_length','2018-11-18 20:46:15.440884'),(14,'auth','0009_alter_user_last_name_max_length','2018-11-18 20:46:15.458496'),(15,'bici_auth','0001_initial','2018-11-18 20:46:15.492950'),(16,'bicis','0001_initial','2018-11-18 20:46:15.771467'),(17,'bici_auth','0002_auto_20181118_1746','2018-11-18 20:46:15.921701'),(18,'sessions','0001_initial','2018-11-18 20:46:15.938583'),(19,'bici_auth','0003_auto_20181118_1916','2018-11-18 22:17:01.039405'),(20,'bici_auth','0004_auto_20181118_1918','2018-11-18 22:18:12.454309'),(21,'bicis','0002_auto_20181118_2050','2018-11-18 23:50:23.414595'),(22,'bici_auth','0005_auto_20181118_2235','2018-11-19 01:35:52.306451'),(23,'bici_auth','0006_auto_20181118_2238','2018-11-19 01:38:27.215321'),(24,'bicis','0003_sucursal_horario_apertura','2018-11-19 02:46:32.249712');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0h9vnn12u5l4sresq1nonposmpqeteys','ZWVhZmE1NTUwOWI4ZmEzZWQ3OWM3YzJmYTJkNzY4YmE0NjYxMjBmMzp7InVzZXJfdHlwZSI6IkVOIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjM3MTUwNmMxNTYyM2ZkMDBhNDhiNmJjODUzYmQ5NzFiNzdmZWQ2YjQifQ==','2018-12-03 01:38:39.660049'),('47mjq83hmsyq8g3fyq5ejb4jefo668c0','NWNlZmIzOGVjNzg2YzhhM2UzOWE5MWE0YjRkZjNhZDE4ZDI2MjM3Nzp7fQ==','2018-12-03 01:34:16.383654'),('gdnvv11mkdicntp794gwncynyjlzo3g8','NWNlZmIzOGVjNzg2YzhhM2UzOWE5MWE0YjRkZjNhZDE4ZDI2MjM3Nzp7fQ==','2018-12-03 01:35:02.707229'),('ulsfozsbk886dlb54j62vxugqci12dk1','NWNlZmIzOGVjNzg2YzhhM2UzOWE5MWE0YjRkZjNhZDE4ZDI2MjM3Nzp7fQ==','2018-12-03 01:36:15.289544'),('yjntqrjx4aafaaiwgro8r5icxcu4tnet','NWNlZmIzOGVjNzg2YzhhM2UzOWE5MWE0YjRkZjNhZDE4ZDI2MjM3Nzp7fQ==','2018-12-02 22:08:55.931741');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-19  2:46:47
