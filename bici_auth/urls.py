from django.urls import path
from bici_auth import views

urlpatterns = [
    path('login/', views.auth_login, name="login"),
]
