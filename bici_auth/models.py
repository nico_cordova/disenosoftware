from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from bici_auth.defines import MANAGER_TYPE_CHOICES, MANAGER_TYPE_DEFAULT
from bicis.models import Sucursal
# Create your models here.

class PerfilUsuario(models.Model):
    user = models.OneToOneField(User,null=True,default=None,on_delete=models.CASCADE)
    telefono = models.CharField(max_length=16,default="Sin Numero")
    rut = models.CharField(max_length=12, default="Sin Rut")
    
    class Meta:
        abstract = True


class Encargado(PerfilUsuario):
    fecha_contrato = models.DateTimeField(default=timezone.now)
    tipo_encargado = models.CharField(max_length=2,choices=MANAGER_TYPE_CHOICES,default=MANAGER_TYPE_DEFAULT)
    sucursal = models.ForeignKey(Sucursal,null=True,on_delete=models.SET_NULL)

    def __str__(self):
        return "{0} {1}".format(self.user.first_name,self.user.last_name)

class Cliente(PerfilUsuario):
    num_tarjeta = models.IntegerField(default=0)
    Mven_tarjeta = models.IntegerField(default=0)
    Aven_tarjeta = models.IntegerField(default=0)
    codigo_seguridad = models.IntegerField(default=0)
    fecha_ingreso = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return "{0} {1}".format(self.user.first_name,self.user.last_name)

