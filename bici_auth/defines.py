# -*- coding: utf-8 -*-
USER_TYPE_CHOICES = (
    ('CL', 'Cliente'),
    ('EN', 'Encargado'),
)
MANAGER_TYPE_CHOICES = (
    ('GE', 'Gerente'),
    ('ET', 'Encargado de tienda'),
)

USER_TYPE_DEFAULT = "CL"
MANAGER_TYPE_DEFAULT = "ET"