from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.
def auth_login(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST["username"]
        password = request.POST["password"]
       
        user = authenticate(
            username=username,
            password=password
        )
        if user != None:
            login(request, user)
            if user.encargado:
                request.session['user_type'] = "EN"
                return HttpResponseRedirect(reverse('index'))
            elif user.cliente:
                request.session['user_type'] = "CL"
                return HttpResponseRedirect(reverse('index'))

        else:
            messages.error(request, 'Usuario y contraseña invalidos. Intentelo nuevamente.')

    data = {}
    template_name = "login.html"
    return render(request, template_name, data)

