from django.contrib import admin
from django import forms
from bici_auth.models import Cliente,Encargado
# Register your models here.

class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = '__all__'


class ClienteAdmin(admin.ModelAdmin):
    form = ClienteForm
    list_display = ['user','rut']

admin.site.register(Cliente, ClienteAdmin)

class EncargadoForm(forms.ModelForm):
    class Meta:
        model = Encargado
        fields = '__all__'


class EncargadoAdmin(admin.ModelAdmin):
    form = EncargadoForm
    list_display = ['user','rut']

admin.site.register(Encargado, EncargadoAdmin)

# Register your models here.
